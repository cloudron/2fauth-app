#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCAL_USERNAME = "cloudron";
    const LOCAL_EMAIL = "admin@cloudron.local";
    const LOCAL_PASSWORD = "changeme";
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;

    const ID = Math.floor((Math.random() * 100) + 1);
    const TWOFA_SERVICE = 'Test 2FA ' + ID;
    const TWOFA_ACCOUNT = 'Test Account ' + ID;
    // base 32 string = test base32 string"
    const TWOFA_ACCOUNT_SECRET = 'ORSXG5BAMJQXGZJTGIQHG5DSNFXGOII=';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    });

    after(function () {
        browser.quit();
    });

    async function saveScreenshot(fileName) {
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${fileName.replaceAll(' ', '_')}-${new Date().getTime()}.png`, screenshotData, 'base64');
    }

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');
        await saveScreenshot(this.currentTest.title);
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function registerUser() {
        await browser.get(`https://${app.fqdn}/register`);
        await browser.sleep(2000);
        await waitForElement(By.id('btnRegister'));
        await browser.findElement(By.id('txtName')).sendKeys(LOCAL_USERNAME);

        await browser.findElement(By.id('emlEmail')).sendKeys(LOCAL_EMAIL);
        await browser.findElement(By.id('pwdPassword')).sendKeys(LOCAL_PASSWORD);
        await browser.sleep(2000);
        await browser.findElement(By.id('btnRegister')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//h1[text()="Authentication"]'));
        await browser.findElement(By.id('btnMaybeLater')).click();
        await browser.sleep(2000);
        await waitForElement(By.id('btnEmailMenu'));
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.id('emlEmail'));

        await browser.findElement(By.id('emlEmail')).sendKeys(LOCAL_EMAIL);
        await browser.findElement(By.id('pwdPassword')).sendKeys(LOCAL_PASSWORD);
        await browser.findElement(By.id('btnSignIn')).submit();

        await browser.sleep(2000);

        await waitForElement(By.id('btnEmailMenu'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated) {
        await browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn + '/login');
        await browser.sleep(2000);

        // next release it can be replaced with "lnkSignWithopenid"
        const oidcBtn = '//a[@id="lnkSignWithOpenID" or @id="lnkSignWithopenid"]';
        await waitForElement(By.xpath(oidcBtn));
        await browser.findElement(By.xpath(oidcBtn)).click();

        await browser.sleep(3000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.id('btnEmailMenu'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(2000);
        await browser.findElement(By.id('btnEmailMenu')).click();
        await browser.sleep(2000);
        await browser.findElement(By.id('lnkSignOut')).click();
        await browser.sleep(2000);
        await browser.switchTo().alert().accept();
        await waitForElement(By.id('emlEmail'));
    }

    async function create2FA() {
        await browser.get(`https://${app.fqdn}/account/create`);

        await browser.sleep(2000);

        await waitForElement(By.id('txtService'));

        await browser.findElement(By.id('txtService')).sendKeys(TWOFA_SERVICE);
        await browser.findElement(By.id('txtAccount')).sendKeys(TWOFA_ACCOUNT);

        // select totp
        await browser.findElement(By.id('btnOtp_typetotp')).click();
        await browser.sleep(2000);

        // ORSXG5BAMJQXGZJTGIQHG5DSNFXGOII=
        await browser.findElement(By.id('txtSecret')).sendKeys(TWOFA_ACCOUNT_SECRET);
        await browser.sleep(2000);

        await browser.findElement(By.id('btnCreate')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//div[contains(., "' + TWOFA_SERVICE + '")]'));
    }

    async function check2FA() {
        await browser.get(`https://${app.fqdn}/accounts`);
        await waitForElement(By.xpath('//div[contains(., "' + TWOFA_SERVICE + '")]'));
    }

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no sso
    it('can install app (no sso)', async function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
        // wait when all settings take a place
        await browser.sleep(20000);
    });
    it('can get app information', getAppInfo);
    it('can register new user', registerUser);
    it('can create 2FA', create2FA);
    it('can check 2FA', check2FA);
    it('can logout', logout);
    it('can login', login);
    it('can check 2FA', check2FA);
    it('can logout', logout);
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // sso
    it('install app (sso)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false));
    it('can create 2FA', create2FA);
    it('can check 2FA', check2FA);
    it('can logout', logout);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check 2FA', check2FA);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check 2FA', check2FA);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check 2FA', check2FA);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync(`cloudron install --appstore-id app.twofauth.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(20000);
    });
    it('can get app information', getAppInfo);

    it('clear cache', clearCache);
    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false));
    it('can create 2FA', create2FA);
    it('can check 2FA', check2FA);
    it('can logout', logout);

    it('can update', async function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check 2FA', check2FA);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
