[0.1.0]
* Initial version

[0.2.0]
* Various fixes to packaging

[0.3.0]
* Add forum url

[0.4.0]
* Update 2FAuth to 5.1.1
* [Full changelog](https://github.com/Bubka/2FAuth/releases/tag/v5.1.0)
* a brand new Admin Panel has arrived
* A user preference to clear search results after copying a code (#300).
* A user preference to return to default group after copying a code (#300).
* The ability to submit a migration text directly in the Import view besides TXT files & QR codes loading (#288).
* An administrator setting to restrict registration to a limited range of email addresses (#250).
* An administrator setting to keep user registration via SSO enabled (#317).

[0.5.0]
* Fix post install message

[1.0.0]
* Update 2FAuth to 5.2.0
* [Full changelog](https://github.com/Bubka/2FAuth/releases/tag/v5.2.0)
* Update base image to 5.0.0
* When installed, 2FAuth now offers shortcuts to common actions.
* User authentication logs (See user management pages in the admin area).
* Two user preferences to control the notifications sent when authentication events occur.
* A user preference to set the timezone applied to dates and times displayed in the app.

[1.1.0]
* Move session files to run time dir
* Use base image 5.0.0

[1.2.0]
* Fix email sending configuration

[1.2.1]
* Add optionalSso option

[1.3.0]
* Update 2FAuth to 5.3.0
* [Full changelog](https://github.com/Bubka/2FAuth/releases/tag/v5.3.0)
* The /up endpoint for health checks (#271).
* A user preference to close the on-screen OTP after a predefined delay
* A user preference to automatically register a 2FA account immediately after a QR code scan. When enabled, there is no need to click the Save button anymore to save the account to the database.
* An admin setting to make SSO the only authentication method available (does not apply to admins). (#368).
* The ability to assign a 2FA account to a specific group directly from the advanced form (#372).
* A new Auth tab in the admin panel to gather settings related to authentication

[1.3.1]
* Update 2FAuth to 5.3.1
* [Full changelog](https://github.com/Bubka/2FAuth/releases/tag/v5.3.1)
* `PROXY_HEADER_FOR_IP` not working as intended
* Base table or view not found: 1146 Table '2fauth.jobs' doesn't exist
* Cannot set `CACHE_DRIVER` and `SESSION_DRIVER` to database

[1.3.2]
* Update 2FAuth to 5.3.2
* [Full changelog](https://github.com/Bubka/2FAuth/releases/tag/v5.3.2)
* Error asking me to log out when using multiple devices, pressing back logs me in anyway


[1.4.0]
* Update 2FAuth to 5.4.0
* [Full Changelog](https://github.com/Bubka/2FAuth/releases/tag/v5.4.0)
* The links in the footer (Settings, \[Admin,] Sign out) have been replaced by the email address of the logged in user. Clicking on this email shows a navigation menu containing the links that were previously visible in the footer. The former display is still available if you don't like the new one, just uncheck the new *Show email in footer* user option in Settings. ([#&#8203;404](https://github.com/Bubka/2FAuth/issues/404))
* Administrators can now configure 2FAuth to register 2FA icons in the database (see the new *Store icons to database* setting in the admin panel). When enabled, existing icons in the local file system are automatically registered in the database. These files are retained and then used for caching purposes only. 2FAuth will automatically re-create cache files if they are missing, so you only have to consider the database when backing up your instance. When disabled, 2FAuth will check that all registered icons in the database have a corresponding local file before flushing out the db icons table. ([#&#8203;364](https://github.com/Bubka/2FAuth/issues/364)).
* The ability to export 2FA accounts as a list of otpauth URIs ([#&#8203;386](https://github.com/Bubka/2FAuth/issues/386)).
* Part of the content of some pages (such as the error page) could be hidden by the footer on small screens.
* New `otpauth` query parameter for the GET operation of path `/api/v1/twofaccounts/export` to force data export as otpauth URIs instead of the 2FAuth json format.

[1.4.1]
* Update 2FAuth to 5.4.2
* [Full Changelog](https://github.com/Bubka/2FAuth/releases/tag/v5.4.2)
* CSP has been turned off (for now) since it breaks the app under Google Chrome. ([#&#8203;417](https://github.com/Bubka/2FAuth/issues/417))
* Fix XSS & SSRF vulnerabilities (thx to the XBOW team).
* Content Security Policy is now available and enable by default. CSP helps to prevent or minimize the risk of certain types of security threats.\

[1.4.2]
* Update 2FAuth to 5.4.3
* [Full Changelog](https://github.com/Bubka/2FAuth/releases/tag/v5.4.3)
* [issue #&#8203;408](https://github.com/Bubka/2FAuth/issues/408) Deleted icon is back after saving from the advanced form
* [issue #&#8203;417](https://github.com/Bubka/2FAuth/issues/417) Login page does not load after v5.4.1 update
* [issue #&#8203;418](https://github.com/Bubka/2FAuth/issues/418) Opening of the footer menu submits the advanced form
* [issue #&#8203;420](https://github.com/Bubka/2FAuth/issues/420) QR codes are cropped on small screens
* [issue #&#8203;421](https://github.com/Bubka/2FAuth/issues/421) Freeze when switching to Manage mode
* [issue #&#8203;423](https://github.com/Bubka/2FAuth/issues/423) Icon for accounts without an icon doesn't exist
* CSS styles are no longer loaded from tailwindcss.com in the `/up` view

[1.4.3]
* Checklist added to CloudronManifest

[1.5.0]
* Update to correct base image 5.0.0

