FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=Bubka/2FAuth versioning=semver extractVersion=^v(?<version>.+)$
ARG TWOFAUTH_VERSION=5.4.3
RUN curl -L https://github.com/Bubka/2FAuth/archive/refs/tags/v${TWOFAUTH_VERSION}.tar.gz | tar zxf - -C /app/code --strip-components 1

RUN composer install --prefer-dist --no-scripts --no-dev

RUN ln -sf /app/data/env /app/code/.env && \
    mv storage storage.orig && ln -sf /app/data/storage /app/code/storage && \
    ln -sf /app/data/storage/app/public /app/code/public/storage && \
    rm -rf /app/code/bootstrap/cache && ln -sf /run/2fauth/bootstrap/cache /app/code/bootstrap/cache

# add nginx config
RUN rm /etc/nginx/sites-enabled/* && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    ln -sf /run/php8.3-fpm.log /var/log/php8.3-fpm.log
COPY nginx/readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
COPY nginx/2fauth.conf /etc/nginx/sites-available/2fauth.conf
RUN ln -s /etc/nginx/sites-available/2fauth.conf /etc/nginx/sites-enabled/2fauth.conf

# add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/2fauth/supervisord.log /var/log/supervisor/supervisord.log

# php
RUN ln -s /app/data/php.ini /etc/php/8.3/fpm/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

ADD start.sh env.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
