#!/bin/bash

set -eu

readonly mysql="mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} "

mkdir -p /run/2fauth/bootstrap/cache /run/2fauth/sessions

if [[ ! -d /app/data/storage ]]; then
    cp -R /app/code/storage.orig /app/data/storage
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [[ ! -f /app/data/env ]]; then
    echo "=> First run"
    cp /app/pkg/env.template /app/data/env

    if [[ -z "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        # get rid of OIDC settings
        sed -e "s/^OPENID_.*//g" -i /app/data/env
    fi

    php artisan key:generate --force --no-interaction
fi

# sessions
[[ -d /app/data/storage/framework/sessions ]] && rm -rf /app/data/storage/framework/sessions
ln -sf /run/2fauth/sessions /app/data/storage/framework/sessions

echo "==> Changing ownership"
chown -R www-data:www-data /app/data /run/2fauth

readonly table_count=$(mysql -NB -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h "${CLOUDRON_MYSQL_HOST}" -P "${CLOUDRON_MYSQL_PORT}" -e "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '${CLOUDRON_MYSQL_DATABASE}';" "${CLOUDRON_MYSQL_DATABASE}" 2>/dev/null)

if [[ "${table_count}" == "0" ]]; then
    echo "==> Initialise 2FAuth"
    gosu www-data:www-data php artisan migrate:refresh --force
    echo "yes" | gosu www-data:www-data php artisan passport:install --force
    gosu www-data:www-data php artisan storage:link
    gosu www-data:www-data php artisan config:cache
else
    echo "==> Upgrade 2FAuth"
    gosu www-data:www-data php artisan cache:clear
    gosu www-data:www-data php artisan config:clear
    gosu www-data:www-data php artisan migrate --force 
    echo "no" | gosu www-data:www-data php artisan passport:install --force
    gosu www-data:www-data php artisan config:cache
    gosu www-data:www-data php artisan route:cache
fi

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    $mysql -e "REPLACE INTO options (options.key, value) VALUES ('enableSso', '{{1}}');";
    $mysql -e "REPLACE INTO options (options.key, value) VALUES ('disableRegistration', '{{1}}');";
    $mysql -e "REPLACE INTO options (options.key, value) VALUES ('keepSsoRegistrationEnabled', '{{1}}');";
fi
$mysql -e "REPLACE INTO options (options.key, value) VALUES ('checkForUpdate', '{{}}');";

# provide default if unset
CLOUDRON_MAIL_FROM_DISPLAY_NAME=${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-2FAuth}

echo "==> Starting 2fauth"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i 2Fauth
